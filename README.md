C++ 项目模板.
使用meson+ninja作为构建系统.

管理依赖有多种方案:

- subprojects 方式, 使用meson原生方式 (项目内)

- 使用 vcpkg 来安装和管理依赖 (全局) (目前更推荐的方案)
